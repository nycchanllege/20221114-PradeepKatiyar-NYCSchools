//
//  Schools.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/17/22.
//

import Foundation

struct Schools: Codable {
    let schoolName:String
    let overview:String
    let email:String?
    let website:String
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case overview = "overview_paragraph"
        case email = "school_email"
        case website = "website"
    }
}
