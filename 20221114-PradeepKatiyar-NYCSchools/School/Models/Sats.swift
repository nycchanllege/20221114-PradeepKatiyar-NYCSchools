//
//  Scores.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/17/22.
//

import Foundation

struct Sats:Codable {
    let schoolName:String
    let numOfSatTesters:String?
    let readingScore:String?
    let mathScore:String?
    let writingScore:String?
    
    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case numOfSatTesters = "num_of_sat_test_takers"
        case readingScore = "sat_critical_reading_avg_score"
        case mathScore = "sat_math_avg_score"
        case writingScore = "sat_writing_avg_score"
    }
}
