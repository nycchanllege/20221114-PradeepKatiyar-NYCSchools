//
//  SchoolsListViewModel.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/15/22.
//

import Foundation

protocol SchoolViewModelProtocol {
    var count: Int { get }
    func getSchoolData(for index: Int) -> Schools?
    func schoolName(for index: Int) -> String?
    func schoolOverview(for index: Int) -> String?
    func schoolEmail(for index: Int) -> String?
    func schoolWebsite(for index: Int) -> String?
    func requestSchoolsList(completed: @escaping ()-> ())
}

class SchoolsListViewModel: SchoolViewModelProtocol {
    
    private let networkService:NetworkService
    private var schools: [Schools] = []
    
    init(service: NetworkService = NetworkManager()) {
        self.networkService = service
    }
}
    
extension SchoolsListViewModel {
    
    func requestSchoolsList(completed: @escaping ()-> ()) {
        let endpoint = APIEndpoints.schoolsList.url
        
        self.networkService.request(url: endpoint) { [weak self] (result: Result<[Schools], NetworkError>) in
            
            switch result {
            case .success(let schools):
                DispatchQueue.main.async {
                    self?.schools = schools
                    completed()
                }
            case .failure(let error):
                // TODO: Error handling
                print(error)
            }
        }
    }
    
    var count: Int {
        return self.schools.count
    }
    
    func getSchoolData(for index: Int) -> Schools? {
        guard index < self.count else { return nil }
        return self.schools[index]
    }
    
    func schoolName(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].schoolName
    }
    
    func schoolOverview(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].overview
    }
    
    func schoolEmail(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].email
    }
    
    func schoolWebsite(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.schools[index].website
    }
    
}

