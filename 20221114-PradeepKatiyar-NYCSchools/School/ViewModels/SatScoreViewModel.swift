//
//  SatScoreViewModel.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/15/22.
//

import Foundation


//MARK- P
protocol SatScoreViewModelProtocol {
    var count: Int { get }
    func getSatData(for index: Int) -> Sats?
    func scoresName(for index: Int) -> String?
    func numOfTesters(for index: Int) -> String?
    func readingScore(for index: Int) -> String?
    func mathScore(for index: Int) -> String?
    func writingScore(for index: Int) -> String?
    func getSatDetails(for index: Int) -> String?
    func getScoresInfo(for index: Int) -> [String?]
    func requestScores()
}

class SatScoreViewModel: SatScoreViewModelProtocol {
    
    private let networkService:NetworkService
    private var satScore: [Sats] = []
    
    init(service: NetworkService = NetworkManager()) {
        self.networkService = service
    }
    
    func requestScores() {
        let endpoint = APIEndpoints.scoresList.url
        
        self.networkService.request(url: endpoint) { [weak self] (result: Result<[Sats], NetworkError>) in
            
            switch result {
            case .success(let scores):
                self?.satScore = scores
            case .failure(let error):
                // TODO: Make more robust error handling
                print(error)
            }
        }
    }
    
    var count: Int {
        return self.satScore.count
    }
    
    func getSatData(for index: Int) -> Sats? {
        guard index < self.count else { return nil }
        return self.satScore[index]
    }
    
    func scoresName(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.satScore[index].schoolName
    }
    
    func numOfTesters(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.satScore[index].numOfSatTesters
    }
    
    func readingScore(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.satScore[index].readingScore
    }
    
    func mathScore(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.satScore[index].mathScore
    }
    
    func writingScore(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return self.satScore[index].writingScore
    }
    
    func getSatDetails(for index: Int) -> String? {
        guard index < self.count else { return nil }
        return """
        \(scoresName(for: index) ?? "N/A")
        \nMath Score: \(mathScore(for: index) ?? "N/A")
        \nReading Score: \(readingScore(for: index) ?? "N/A")
        \nWriting Score: \(writingScore(for: index) ?? "N/A")
        """
    }
    
    func getScoresInfo(for index: Int) -> [String?] {
        guard index < self.count else { return [] }
        let name = scoresName(for: index)
        let math = mathScore(for: index)
        let reading = readingScore(for: index)
        let writing = writingScore(for: index)
        
        return [name, math, reading, writing]
    }
    
}
