//
//  ScoreDetailsViewController.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/17/22.
//

import Foundation
import UIKit

class ScoreDetailsViewController: UIViewController {
    @IBOutlet weak var scoreDetailsLabel: UILabel!
    
    var satsDetail:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if !satsDetail!.isEmpty {
            scoreDetailsLabel.text = satsDetail
        }
    }
    
}
