//
//  SchoolDetailViewController.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/17/22.
//

import Foundation
import UIKit

class SchoolDetailViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var schoolDetail: Schools?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.nameLabel.text = schoolDetail?.schoolName
        self.overviewLabel.text = schoolDetail?.overview
    }
}
