//
//  SchoolsListTableViewController.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/15/22.
//

import Foundation
import UIKit

class SchoolsListTableViewController: UITableViewController {
    
    @IBOutlet weak var SegmentSelector: UISegmentedControl!
    var viewModel: SchoolsListViewModel!
    
    lazy var schoolsViewModel:SchoolViewModelProtocol = SchoolsListViewModel()
    lazy var scoresViewModel:SatScoreViewModelProtocol = SatScoreViewModel()
    let schoolDetailIdentifier = "SchoolDetailSegue"
    let scoreDetailIdentifier = "ScoreDetailSegue"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = SchoolsListCell.height
        tableView.rowHeight = UITableView.automaticDimension
        
        self.schoolsViewModel.requestSchoolsList {
            self.reload()
        }
        self.scoresViewModel.requestScores()
        
    }
    
    // MARK: - Reload Table View
    func reload() {
        tableView.reloadData()
    }
    
    // MARK: - Segment Click
    @IBAction func selectorPressed(_ sender: Any) {
        reload()
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension SchoolsListTableViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.SegmentSelector.selectedSegmentIndex == 0 {
            return schoolsViewModel.count
        } else {
            return scoresViewModel.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SchoolsListCell.reuseIdentifier, for: indexPath) as! SchoolsListCell
        
        if self.SegmentSelector.selectedSegmentIndex == 0 {
            cell.schoolData = self.schoolsViewModel.getSchoolData(for: indexPath.row)
        } else {
            cell.satsData = self.scoresViewModel.getSatData(for: indexPath.row)
        }
        
        return cell
    }
    
    
    override func tableView(_ tableView:UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.SegmentSelector.selectedSegmentIndex == 0 {
            performSegue(withIdentifier: schoolDetailIdentifier, sender: self)
        } else {
            performSegue(withIdentifier: scoreDetailIdentifier, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == schoolDetailIdentifier {
            if let destination = segue.destination as? SchoolDetailViewController {
                destination.schoolDetail = self.schoolsViewModel.getSchoolData(for: tableView.indexPathForSelectedRow?.row ?? 0)
            }
        }else if segue.identifier == scoreDetailIdentifier {
            if let destination = segue.destination as? ScoreDetailsViewController {
                destination.satsDetail = self.scoresViewModel.getSatDetails(for: tableView.indexPathForSelectedRow?.row ?? 0)
            }
        }
    }
}
