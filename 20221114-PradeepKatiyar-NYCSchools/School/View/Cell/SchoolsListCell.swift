//
//  SchoolsListCell.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/15/22.
//

import Foundation
import UIKit

class SchoolsListCell: UITableViewCell {
    
    static let reuseIdentifier = String(describing: SchoolsListCell.self)
    static let height = CGFloat(130)
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    var schoolData: Schools? {
        didSet {
            titleLabel.text = schoolData?.schoolName
            overviewLabel.text = schoolData?.overview
        }
    }
    
    var satsData: Sats? {
        didSet {
            titleLabel.text = satsData?.schoolName
            overviewLabel.text = satsData?.numOfSatTesters
        }
    }
}

