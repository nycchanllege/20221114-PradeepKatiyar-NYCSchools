//
//  APIEndpoints.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/15/22.
//

import Foundation

// MARK: - Network API Path
enum APIEndpoints {
    
    case schoolsList
    case scoresList
    
    var url: URL? {
        switch self {
        case .schoolsList:
            return URL(string: "https://data.cityofnewyork.us/resource/s3k6-pzi2.json")
        case .scoresList:
            return URL(string: "https://data.cityofnewyork.us/resource/f9bf-2cp4.json")
        }
    }
}




