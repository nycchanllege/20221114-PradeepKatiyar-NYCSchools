//
//  NetworkManager.swift
//  20221114-PradeepKatiyar-NYCSchools
//
//  Created by Pradeep Katiyar on 11/17/22.
//

import Foundation


protocol NetworkService {
    func request<T: Decodable>(url: URL?, completion: @escaping (Result<T, NetworkError>) -> Void)
}

public enum NetworkError: Error {
    case invalidURL
    case badData
    case invalidStatusCode(Int)
    case decodeError(Error)
    case other(Error)
}

class NetworkManager {
    
    let session: URLSession
    
    init(session: URLSession = URLSession.shared) {
        self.session = session
    }
}

extension NetworkManager: NetworkService {
    
    func request<T>(url: URL?, completion: @escaping (Result<T, NetworkError>) -> Void) where T : Decodable {
        
        guard let url = url else {
            completion(.failure(.invalidURL))
            return
        }
        
        self.session.dataTask(with: url) { data, response, error in
            
            if let error = error {
                completion(.failure(.other(error)))
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse,
               !(200..<300).contains(httpResponse.statusCode) {
                completion(.failure(.invalidStatusCode(httpResponse.statusCode)))
                return
            }
            
            guard let data = data else {
                completion(.failure(.badData))
                return
            }
            
            do {
                let model = try JSONDecoder().decode(T.self, from: data)
                completion(.success(model))
            } catch {
                completion(.failure(.decodeError(error)))
            }
        }.resume()
    }
}
